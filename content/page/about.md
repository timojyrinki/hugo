---
title: About this blog
subtitle: 
comments: false

---

This blog is to post some experiences from (open)SUSE development.

I started my Linux user with SUSE in 1997, and after a long hiatus in other distributions I've been using SUSE again since 2018.


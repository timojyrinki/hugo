---
title: Disabling broken webcam on demand
subtitle: Random notes
date: 2021-10-29T14:00:00+03:00
tags: ["camera", "brokenhw"]
type: post
---

This is more like a self-written notes post about a problem I'm facing, since my laptop's web camera is starting to deteriorate. I'll update the post if I find more useful bits of how to tweak with USB. For the first few weeks I was suspecting a Tumbleweed problem, but eventually I booted up Ubuntu LTS from USB stick and managed to see the problem also there.

dmesg goes like follows
```
[46253.245741] input: Integrated_Webcam_HD: Integrate as /devices/pci0000:00/0000:00:14.0/usb1/1-5/1-5:1.2/input/input195
[46254.233519] usb 1-5: USB disconnect, device number 97
[46254.234502] uvcvideo 1-5:1.1: Failed to resubmit video URB (-19).
[46254.687974] usb 1-5: new high-speed USB device number 98 using xhci_hcd
[46254.895417] usb 1-5: New USB device found, idVendor=0bda, idProduct=58f4, bcdDevice=72.79
[46254.895422] usb 1-5: New USB device strings: Mfr=3, Product=1, SerialNumber=2
[46254.895424] usb 1-5: Product: Integrated_Webcam_HD
[46254.895425] usb 1-5: Manufacturer: XXXXXXXXXXXXXXXXXXXXXXX
[46254.895426] usb 1-5: SerialNumber: XXXXXXXXXXXX
[46254.899284] usb 1-5: Found UVC 1.50 device Integrated_Webcam_HD (0bda:58f4)
[46254.906277] input: Integrated_Webcam_HD: Integrate as /devices/pci0000:00/0000:00:14.0/usb1/1-5/1-5:1.0/input/input196
[46254.907449] usb 1-5: Found UVC 1.50 device Integrated_Webcam_HD (0bda:58f4)
[46254.909691] input: Integrated_Webcam_HD: Integrate as /devices/pci0000:00/0000:00:14.0/usb1/1-5/1-5:1.2/input/input197
[46256.877289] usb 1-5: USB disconnect, device number 98
[46256.878499] uvcvideo 1-5:1.1: Failed to resubmit video URB (-19).
[46256.882484] uvcvideo 1-5:1.1: Failed to resubmit video URB (-19).
[46256.886482] uvcvideo 1-5:1.1: Failed to resubmit video URB (-19).
[46256.890482] uvcvideo 1-5:1.1: Failed to resubmit video URB (-19).
[46256.894500] uvcvideo 1-5:1.1: Failed to resubmit video URB (-19).
[46258.187945] usb usb1-port5: Cannot enable. Maybe the USB cable is bad?
[46259.167940] usb usb1-port5: Cannot enable. Maybe the USB cable is bad?
[46259.167971] usb usb1-port5: attempt power cycle
[46260.235968] usb usb1-port5: Cannot enable. Maybe the USB cable is bad?
```

I tried opening the laptop and using some duct tape to the cable running to the camera, but while it seemed better at first, it seems it now continues to be as it was before. The camera still works from time to time, especially if I don't touch the laptop by any means, using external keyboard and mouse. But when it breaks, it sometimes causes a stall of a few seconds.

I have the option to disable web camera in UEFI setup, but I'd rather find a way to power it off and on during runtime. I've tried eg

```bash
echo '1-5' | sudo tee /sys/bus/usb/drivers/usb/unbind
```

But the device does not seem to exist. Meanwhile, I mixed up the bus/device/port concepts since Bus 001 Device 005 in lsusb is actually the Bluetooth device:
```
# Bus 001 Device 005: ID 0489:e0a2 Foxconn / Hon Hai
```

But at least I managed to try out the usb_modeswitch trick found from the web, which I'd like to apply to the webcam too:
```bash
> sudo usb_modeswitch -v 0489 -p e0a2 --reset-usb
Look for default devices ...
 Found devices in default mode (1)
Access device 005 on bus 001
Get the current device configuration ...
Current configuration number is 1
Use interface number 0
 with class 224
Warning: no switching method given. See documentation
Reset USB device .
 Device was reset
-> Run lsusb to note any changes. Bye!
```


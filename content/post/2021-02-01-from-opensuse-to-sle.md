---
title: Getting a package from openSUSE to SLE
subtitle: Contributing to SLE as a community member
date: 2021-02-01T18:54:00+02:00
tags: ["opensuse", "sle", "closingtheleapgap"]
type: post
---

I was looking forward to updating a package ([enchant](https://github.com/AbiWord/enchant)) with a backported patch from upstream and wanted it to be included in both SLE offerings and openSUSE. I'm used to working within the community from the past, so even though I'm a SUSE employee I wanted to contribute without using anything internal.

I filed a [bug](https://bugzilla.opensuse.org/show_bug.cgi?id=1178489) and did a [request](https://build.opensuse.org/request/show/846328) to first get the patch into Tumbleweed via GNOME:Factory. That was easy, but then I looked on how to get the same patch into the stable releases. Since openSUSE and SUSE are now coming closer, I found documentationa about the [Jump](https://en.opensuse.org/Portal:Jump) and mailing lists described some documents for example that there either is or is going to be a new redirector service, and [suggestion was to target openSUSE:Jump:15.2](https://lists.opensuse.org/opensuse-factory/2020-08/msg00075.html) at one point.

Eventually I found out that since 15.3 is when SLE & openSUSE really closes the leap gap, Jump is already history and currently the best target is to use openSUSE:Leap:15.3. I was actually told by someone that it wouldn't work, but... it did!

In practice my [Leap request](https://build.opensuse.org/request/show/853741) (I really submitted it to openSUSE:Leap:15.3!) was automatically detected to be about a package that openSUSE inherits from SLE, so the target was switched to SLE 15-SP2 on the fly. Secondly, after some time a person managing these kind of requests copied over my public request to be usable inside SLE process, and then an official SLE 15-SP2 update request was created automatically. It passed QA in the beginning of January, and was made available to 15-SP2 users! Luckily 15-SP3 has no deviation for this package from 15-SP2, so it's automatically also there, and that means openSUSE Leap 15.3 is going to have the patched package automatically as well.

Finally, also without my manual intervention there was a [request](https://build.opensuse.org/request/show/860353) to get it to Leap 15.2 as well, finalizing fixing all relevant releases (15.1 is not affected by the bug).

All in all, even though it feels the documentation is a bit lacking and there could be more feedback or clearer visibility on a higher level, the functionality of the process was really good. Now that I know a bit more, I can try to improve the wiki a bit.


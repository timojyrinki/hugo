---
title: My containerized websites
subtitle: Having fun with technologies not part of my day job
date: 2023-11-10T12:00:00+02:00
tags: ["website", "nginx", "proxy", "docker", "docker-compose", "apache", "letsencrypt", "certbot"]
type: post
---

Coincidentally I have had multiple needs for serving websites from behind containers instead of directly from the host. After some practicing, which can be also described as ”hacking without carefully documenting what I did”, I decided not to explicitly describe my setup but how I achieved (towards) my goal of serving my web sites (3 on the same server) from behind containers. This will enable me to consider using things like PHP or other notorious pieces of software thanks to a level of sandboxing.

```Here in this rant paragraph I note that I'm not overtly fan of pulling random images from the web even if not quite as bad as using curl-sudo ”install method”. Containers are at least sandboxed, but coming from Linux distribution background perspective I somewhat dislike all software vendors acting as OS image providers. I will need to learn more about the aspects of where the images come from, what is the trust model, and what happens if eg Official Nginx (tm) image or Official Certbot (tm) image or Docker Hub gets compromised. I do mostly trust Debian, SUSE, Ubuntu etc infrastructure, so it will take some time until I construct my trust towards alternatives. One possible option could be of course to use my pre-trusted vendors via some alternative image registry, and do a bit more work with Ansible for example to utilize them instead of trusting Nginx or Certbot via Docker Hub.```

My setup is looking like ending up like this, involving docker-compose, nginx as a proxy, several web host containers, and a Certbot container that is used to renew certificates via a cronjob:

![A diagram showing docker-compose handling the proxy, websites and certbot, with a humorous hint of hoping to ansible it at some point](containerized_website.png)

But before I ended up with that I said hello to Search Engine. One result showed the general idea of [sharing directories](https://github.com/wmnnd/nginx-certbot/blob/master/docker-compose.yml). Another reminded how to [serve acme-challenge](https://pentacent.medium.com/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71) in nginx since I've been mainly Apache user. There were [several](https://www.linkedin.com/pulse/use-apache-ssllets-encrypt-docker-leon-sczepansky) guides about Apache and Let's encrypt too, without proxy. I probably read [this](https://gist.github.com/maxivak/4706c87698d14e9de0918b6ea2a41015) when figuring out the actual nginx proxy setup. At some point I needed to check that [nginx is ”nginx”](https://hub.docker.com/_/nginx) while [certbot is ”certbot/certbot”](https://hub.docker.com/r/certbot/certbot). I was happy to find [certbot command line option ideas](https://blog.jarrousse.org/2022/04/09/an-elegant-way-to-use-docker-compose-to-obtain-and-renew-a-lets-encrypt-ssl-certificate-with-certbot-and-configure-the-nginx-service-to-use-it/) although I learned the [hard way](https://letsencrypt.org/docs/failed-validation-limit/) to use ```--dry-run``` as well especially in the case of forgetting ```restart``` option in docker-compose.yml. I did not need any kind of bootstrap phase since I already had existing Let's Encrypt certificates.

Finally, in practice what I have done so far is a proof that it works and can be used as drop in replacement for my on-host Apache. I have served all of the web sites from inside a container (pointed to the same content as the on-host), and Certbot container works to renew the certificate, replacing [acme-tiny](https://github.com/diafygi/acme-tiny/) I have used before. Currently however the setup is not complete for simultaneous serving of all the 3 sites, cronjobs are not set up (or tested to be functional). I can't have my on-host Apache and containerized Nginx proxy competing for ports 80 and 443 at the same time, so I can really enable the setup only when everything is fully complete. It's nice however how easy switching between the two already is!

Finally², I can't say it would be 100% complete even then! At some point in the past I have *largely but not completely* ”ansibled” my host when I sadly moved from a really old quad-core ARM server to x86_64 server. I like different architectures, but the ARM servers were decommissioned by Scaleway. Anyway, regarding Ansible this kind of containerization obviously increases ”Ansible debt” further by a fair amount, and that would be a different story to get that fully done...


But it has been some nice hacking!

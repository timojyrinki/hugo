---
title: GNOME Dynamic Triple Buffering patch on openSUSE
subtitle: Double the fluidness on your Intel laptop or RPI
date: 2022-04-28T15:30:00+03:00
tags: ["performance", "gnome", "triplebuffering", "danielvanvugt", "speed", "opensuse", "tumbleweed"]
type: post
---

I've always, or at least ever since the development of the [iconic Nokia N9](https://www.gsmarena.com/flashback_nokia_n9-news-40454.php) and the projects I was working on at the time, wanted "60 fps" silky smooth behavior from both phones and computers, and learned to be sensitive to that. GNOME has been fighting back a bit on that front for several years though on HiDPI displays, with also regressing at least on openSUSE a bit earlier which I was [unable to pinpoint exact reason to](https://bugzilla.opensuse.org/show_bug.cgi?id=1112824#c197). A combination of power management options helped, as have been kernel and GNOME fixes later on, so I have been mostly ok but still not happy.

This changed last year, when Daniel van Vugt started offering his awesome work on [triple buffering in GNOME](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1441), which ensures correct amount of power budget to be used for what's needed for a smooth UI. I [backported / adapted the patch to GNOME 40 on Tumbleweed](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1441#note_1257807) first, then later GNOME 41. There were a couple of bugs left and Wayland support worked worse at the time, but I still was so happy with the performance that I couldn't imagine anymore using GNOME without the patch set.

Now with GNOME 42 the patch set is mature, and I'm using it also on Wayland without obvious issues. It's great on my [laptop's internal 4K display](https://timojyrinki.gitlab.io/hugo/post/2022-01-26-dell-xps-13-developer-edition-ubuntu-opensuse/), and on my WQHD (2560x1440) desktop screen. Notably, the patch set is enabled by default in the new Ubuntu 22.04 LTS long-term supported version, after being in development for 1.5 years. I really hope it will be merged upstream soon, but meanwhile we have the next best option - a patch set that directly applies on top of GNOME 42.

To install it from my OBS repository (note: I will not use that repository for other packages despite the generic name):

```bash
zypper ar https://download.opensuse.org/repositories/home:/tjyrinki_suse:/branches:/openSUSE:/Factory/openSUSE_Tumbleweed/home:tjyrinki_suse:branches:openSUSE:Factory.repo
zypper up
```

The repository automatically updates to the latest changes whenever there's a new Mutter in Tumbleweed. I will also likely continue to update the repository either with the patch set from upstream, or if rebasing becomes more difficult I can also check the patch set changes applied on top of Ubuntu's GNOME 42.

For smoother future!


---
title: Unboxing Dell XPS 13 - openSUSE Tumbleweed alongside preinstalled Ubuntu
subtitle: A look at the 2021 model of Dell XPS 13 - available with Linux pre-installed
date: 2022-01-26T14:30:00+02:00
tags: ["unboxing", "xps13", "linuxdesktop", "ubuntu", "opensuse", "tumbleweed"]
type: post
---

I received a new laptop for work - a Dell XPS 13. Dell has been long famous for offering certain models with pre-installed Linux as a supported option, and opting for those is nice for moving some euros/dollars from certain PC desktop OS monopoly towards Linux desktop engineering costs. Notably Lenovo also offers Ubuntu and Fedora options on many models these days (like Carbon X1 and P15 Gen 2).

![Unboxing Dell XPS 13 - black box](/dellxps13_1.jpg)
![Unboxing Dell XPS 13 - opened box](/dellxps13_2.jpg)
![Unboxing Dell XPS 13 - accessories and a leaflet about Linux support](/dellxps13_3.jpg)
![Unboxing Dell XPS 13 - laptop lifted from the box, closed](/dellxps13_4.jpg)
![Unboxing Dell XPS 13 - laptop with lid open](/dellxps13_5.jpg)
![Unboxing Dell XPS 13 - Ubuntu running](/dellxps13_6.jpg)
![Unboxing Dell XPS 13 - openSUSE running](/dellxps13_7.jpg)

Obviously a smooth, ready-to-rock Ubuntu installation is nice for most people already, but I need openSUSE, so after checking everything is fine with Ubuntu, I continued to install openSUSE Tumbleweed as a dual boot option. As I'm a funny little tinkerer, I obviously went with some special things. I wanted:

- Ubuntu to remain as the reference supported OS on a small(ish) partition, useful to compare to if trying out new development versions of software on openSUSE and finding oddities.
- openSUSE as the OS consuming most of the space.
- LUKS encryption for openSUSE without LVM.
- ext4's new fancy 'fast_commit' feature in use during filesystem creation.
- As a result of all that, I ended up juggling back and forth installation screens a couple of times (even more than shown below, and also because I forgot I wanted to use encryption the first time around).

First boots to pre-installed Ubuntu and installation of openSUSE Tumbleweed as the dual-boot option:
<!-- {{ video src="dellxps13video" >}} add "<" after {{ -->
(if the embedded video is not shown, use a [direct link](https://timojyrinki.gitlab.io/hugo/post/2022-01-26-dell-xps-13-developer-edition-ubuntu-opensuse/dellxps13video.webm))

Some notes from the openSUSE installation:

- openSUSE installer's partition editor apparently does not support resizing or automatically installing side-by-side another Linux distribution, so I did part of the setup completely on my own.
- Installation package download hanged a couple of times, only passed when I entered a mirror manually. On my TW I've also noticed download problems recently, there might be a problem with some mirror I need to escalate.
- The installer doesn't very clearly show encryption status of the target installation - it took me a couple of attempts before I even noticed the small "encrypted" column and icon (well, very small, see below), which also did not spell out the device mapper name but only the main partition name. In the end it was going to do the right thing right away and use my pre-created encrypted target partition as I wanted, but it could be a better UX. Then again I was doing my very own tweaks anyway.
- Let's not go to the details why I'm so old-fashioned and use ext4 :)
- openSUSE's installer does not work fine with HiDPI screen. Funnily the tty consoles seem to be fine and with a big font.
- At the end of the video I install the two GNOME extensions I can't live without, Dash to Dock and Sound Input & Output Device Chooser.

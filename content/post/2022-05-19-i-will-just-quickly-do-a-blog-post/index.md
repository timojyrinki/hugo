---
title: I will just quickly do a blog post...
subtitle: Fun with modern computing
date: 2022-05-19T15:25:00+03:00
tags: ["blogging", "hugo", "changingplatforms", "planet"]
type: post
---

I got ”inspired” by my writing of the previous blog post, and wrote in a channel about my experience some time ago. So why not also do a blog post about doing a blog post :)

So... I was planning to use GitLab's Pages feature via my Hugo fork as usual to push it through. So like, concentrate on writing and do a publish, right, like in good old times? I did so, but all I got both locally and in remote pipeline was stuff like...

```bash
"ERROR  render of "page" failed: execute of template failed: template: _default/single.html:3:5: executing "_default/single.html" at <partial "head.html" .>: error calling partial: "/git/themes/beautifulhugo/layouts/partials/head.html:33:38": execute of template failed: template: partials/head.html:33:38: executing "partials/head.html" at <.URL>: can't evaluate field URL in type *hugolib.pageState"
```

Quite helpful, right, and points to the right direction to quickly resolve the issue?

With some googling it turned out everything has changed and stuff is now [broken](https://gitlab.com/pages/hugo/-/issues/66/), and meanwhile the approach was [re-done](https://gitlab.com/pages/hugo/-/merge_requests/165/diffs) (in that and following commits) making everyone's forks incompatible.

Well, I merged back modifications from there, and noticed there's another problem, documented as "*Generics were introduced in Go 1.18, and they broke some features in the newer versions of Hugo. For now, if you use hugo or hugo_extended versions 0.92.2 or later, you might encounter problems building the website.*". So I went ahead and hard-coded to ages old hugo version, although it then later was revealed it seems to still work for me with newer one as well. The issue is though mentioned in README, and probably leads many to think that's a cause for their whatever problem, and the issue is still [open](https://gitlab.com/pages/hugo/-/issues/69).

Eventually I got everything working and figured out the settings which naturally had changed. Then I wanted my RSS to be like it was before, non-cut since I knew the cut version worked poorly in [Planet.o.o](https://planet.opensuse.org/). *Naturally* this meant I needed to fork the whole theme, declare it as a module of my own to not clash over the upstream, and add the one custom XML file I had hacked together from various sources for the previous theme. This was actually a very pleasant surprise in the end – I randomly guessed I'd paste the file under layouts in my theme fork, and it simply worked the same way it worked in the previous theme!

So problem solved! I think?

Now my emotions regarding Modern Technology were affected by the following, last part too even though it's not related to Hugo. So, in the end I had everything setup and working, but my post didn't appear to Planet.o.o. Turns out Planet was also broken, ignoring last 15% of blogs, and I needed to Ruby my way to figure out the [workaround](https://github.com/openSUSE/planet-o-o/pull/51) 😁

All good in the end, but it turns out there's no substitute for stable platforms, good documentation and solid user experience even in the days of possibilities of doing git forks, using great languages like Go, having container running pipelines for testing etc - the error messages might be just as unhelpful as ever.

**Meanwhile...** I updated an association's 10 year old PHP based Wordpress site (that I got access rights from my fellows who are non-techies) containing the most horrible custom hacks I have seen and no idea who created the site back then – and *that* site simply upgraded to very latest security patched Wordpress version with zero problems.

---

**Postscriptum**: I was not able to publish this blog post, since... you know, stuff had broken again.
```bash
panic: languages not configured
goroutine 1 [running]:
github.com/gohugoio/hugo/commands.(*commandeer).loadConfig(0xc0003cc1e0)
	/root/project/hugo/commands/commandeer.go:374 +0xb3c
github.com/gohugoio/hugo/commands.newCommandeer(0x0, 0x0, 0x0, 0xc0003cc0f0, {0x28740a0?, 0xc000010d38}, 0x0, {0x0, 0x0, 0x0})
```

Helpful error messages to rescue again! After staring at awe at the above for some time... remember the warning about Go generics and Hugo versions? Looks like it came into "fruition", so it was time to hard-code the Hugo version down now. And you are now enjoying the result!

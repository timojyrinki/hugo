---
title: Switching from Docker to Podman
subtitle: Future proofing my server deployment
date: 2024-11-22T09:50:00+02:00
tags: ["docker", "podman", "compose", "website", "service", "migration"]
type: post
---

## The State at the Beginning

Earlier I had made progress on my server to serve different websites from inside containers, including refreshing SSL certificates. But the server started to be aging, and for both learning and future proofing purposes I started looking at migrating to something newer. And, I never really finished all the old work anyway. Instead I have a new container in use that has SSH running inside, that should be migrated as well.

## OS / Software Upgrades

First of all I updated the OS, to have the latest or at least newer Podman available, as I tend to prefer the distro version to any special repositories. I encountered a few problems with disk space running out a few times, but managed to sort out those issues without rendering the server unusable.

## From Docker to Podman

The main topic was switching from Docker to Podman. That involved reading about *docker-compose* vs *podman-compose*, the different formats for those two, *quadlets*, *pods*... so many options, and interestingly many of those have been in varying states of development for the past years, so I'm glad I haven't been too fast on making this switch.

I have liked docker compose. Creating pods from CLI is certainly not interesting for me so not worth the effort. Quadlets would be elegant, but I yearn for something more automatically created from a single definition for quadlets like the [podlets](https://github.com/k9withabone/podlet), which however are not to be merged into podman upstream. Lots of interesting discussion on which ones of these are deprecated, no longer hot etc.

I checked that indeed it seemed I can use podman as a drop-in replacement for Docker, using the docker-compose format. However, the idea to move to native podman-compose format was hindered by comments like ”compose is no longer the way to go with Podman” 🤔

## Reality Check

At this point I decided to actually test this ”drop-in replacement” I mentioned seemed working, and while the container was indeed running without a hitch, I found out that .. well, the services didn't work. So before I got too excited about rewriting everything in something new, I went towards actually figuring out what are the problems currently.

That took quite a bit of time, but eventually I was back to the status before starting. I spent a bit too much time assuming the problem was with podman configuration, while at the end I needed correct firewall routings to the podman network interface, something that wasn't explicitly required before with docker and earlier OS version.

Problem solved! Except that now SSH to container worked, but some SSH features did not work... again, some time was spent until I found out that Podman has dropped AUDIT_WRITE capability from default settings at some point, mostly needed by SSH. Problem Solved! Except that even though there was a change in error messages, it still didn't work. AUDIT_CONTROL or a workaround is needed too, for obvious-to-anyone [reasons](https://github.com/containers/podman/issues/13012) (a nice read, plot thickens etc).

At this point I thought that rewriting anything more would just complicate things, and it'd be better to make the current solution to ”actually work”. So far I had played with the single container that was already in use, but I knew there'd be a bigger effort to combine that with the containerized nginx proxy and let's encrypt that were not yet running in a container by default, running on host instead, and having all of those work together.

## At the End

For my use on this single server case, I found that actually podman-compose with the compatible docker format is not too bad thing to use. I have a more recent platform in use, Podman even in rootful mode is somewhat more secure than Docker, and I have solved issues in integrating this migrated container and some of the other services working in another migrated container too. Most importantly, at the end I had not created more problems than I solved, I have version controlled configuration, improved Ansible scripts and functional backups for re-deployment. Given the time restrictions I have for working on things like these, it's better to have something that a) works, b) is re-deployable than something that is using another technology but is more hackish.

While I could move from podman-compose to quadlets, that would actually make things more complex at least in the current state of things in case I'd like to move to kubernetes. [compose -> quadlets](https://github.com/containers/podlet) and [compose -> kubernetes](https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/) seem quite straightforward, but quadlets -> kubernetes is a bit different topic. Not that I'd really have any use for kubernetes, but for learning purposes it could be nice to keep the option. Maybe using .kube files with quadlets could be ok though.

A separate project would be also to switch to native Podman compose YAML format to strive towards rootless containers.

---
title: Working and warming up cats
subtitle: How to disable internal keyboard/touchpad when a cat arrives
date: 2021-12-14T09:00:00+02:00
tags: ["evtest", "inputdevices", "cats"]
type: post
---

I'm using an external keyboard (<span style="color:red">**1**</span>) and mouse (<span style="color:red">**2**</span>), but the laptop lid is usually still open for better cooling. That means the internal keyboard (<span style="color:red">**3**</span>) and touchpad (<span style="color:red">**4**</span>) – made of comfortable materials – are open to be used by a cat searching for warmth (<span style="color:red">**7**</span>), in the obvious "every time" case that a normal non-heated nest (<span style="color:red">**6**</span>) is not enough.

![A work desktop with a cat problem](/workdesktop_catproblem.jpg)

The problem is, everything goes chaotic at that point in the default configuration. The solution is to have quick shortcuts in my Dash to Dock (<span style="color:red">**8**</span>) to both disable (<span style="color:red">**10**</span>) and enable (<span style="color:red">**9**</span>) keyboard and touchpad at a very rapid pace.

It is to be noted that I'm not disabling the touch screen (<span style="color:red">**5**</span>) by default, because most of the time the cat is not leaning on it – there is also the added benefit that if one forgets about the internal keyboard and touchpad disabling and detaches the laptop from the USB-C monitor (<span style="color:red">**11**</span>), there's the possibility of using the touch screen and on-screen keyboard to type in the password and tap on the keyboard/touchpad enabling shortcut button again. If also touch screen was disabled, the only way would be to go back to an external keyboard or reboot.

So here are the scripts. First, the disabling script (pardon my copy-paste use of certain string manipulation tools):
```bash
dconf write /org/gnome/desktop/peripherals/touchpad/send-events "'disabled'"
sudo killall evtest
sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "AT Translated Set 2 keyboard" | tail -n 1 | sed 's/.*\/dev/\/dev/') &
sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "Dell WMI" | tail -n 1 | sed 's/.*\/dev/\/dev/') &
sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "Power" | grep Kernel | tail -n 1 | sed 's/.*\/dev/\/dev/') &
sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "Power" | grep Kernel | head -n 1 | sed 's/.*\/dev/\/dev/') &
sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "Sleep" | grep Kernel | tail -n 1 | sed 's/.*\/dev/\/dev/') &
sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "HID" | grep Kernel | head -n 1 | sed 's/.*\/dev/\/dev/') &
sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "HID" | tail -n 1 | sed 's/.*\/dev/\/dev/') &
#sudo evtest --grab $(sudo libinput list-devices | grep -A 1 "ELAN" | tail -n 1 | sed 's/.*\/dev/\/dev/') # Touch screen
```

And the associated ~/.local/share/applications/disable-internal-input.desktop:

```bash
[Desktop Entry]
Version=1.0
Name=Disable internal input
GenericName=Disable internal input
Exec=/bin/bash -c /home/timo/Asiakirjat/helpers/disable-internal-input.sh
Icon=yast-keyboard
Type=Application
Terminal=false
Categories=Utility;Development;
```

Here's the enabling script:

```bash
dconf write /org/gnome/desktop/peripherals/touchpad/send-events "'enabled'"
sudo killall evtest
```

and the desktop file:

```bash
[Desktop Entry]
Version=1.0
Name=Enable internal input
GenericName=Enable internal input
Exec=/bin/bash -c /home/timo/Asiakirjat/helpers/enable-internal-input.sh
Icon=/home/timo/.local/share/icons/hicolor/scalable/apps/yast-keyboard-enable.png
Type=Application
Terminal=false
Categories=Utility;Development;
```

With these, if I sense a cat or am just proactive enough, I press *Super+9*. If I'm about to detach my laptop from the monitor, I press *Super+8*. If I forget the latter (usually this is the case) and haven't yet locked the screen, I just tap the enabling icon on the touch screen.

---
title: Best wget options to fully mirror a site
subtitle: 
date: 2023-10-25T16:00:00+03:00
tags: ["mirror", "website", "wget"]
type: post
---

Lately I needed to mirror a website as fully as possible, and ended up researching a bit more than my previous times I've done so. Here I'm just dropping a note that I ended up doing the following:

```bash
wget -mkxp --adjust-extension -e robots=off https://myurl.com/
```

Here -m is:
```
-r -N -l inf --no-remove-listing
```
or in long form:
```
--recursive --timestamping --level inf --no-remove-listing
```

and the rest ie -kxp are, in the same order
```
--convert-links --force-directories --page-requisites
```


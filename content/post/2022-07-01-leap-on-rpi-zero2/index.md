---
title: Camera use on openSUSE Leap on Raspberry Pi Zero 2
subtitle: Checking if I should make a Leap
date: 2022-07-01T09:20:00+03:00
tags: ["opensuse", "raspberrypi", "zero2", "leap"]
type: post
---

One thing I wanted to investigate during the Hackweek was trying out whether openSUSE Leap would offer my Raspberry Pi Zero 2 a nice and stable option for motion detection camera recording. I have had RPi 3 Model A+ doing this for a longer time, and a Zero model before that, but the newer RPi Zero 2 has been a bit unstable for so far unknown reason. There are also some unoptimal combinations of too old or too fresh software in the official Raspberry Pi OS releases. You can read more in my hackweek [project](https://hackweek.opensuse.org/21/projects/migrate-from-raspbian-to-opensuse-aarch64-for-motion-detection-camera) page

![RPi - initial setup over HDMI](rpi1.jpg)

Some of the actions I did over time to change from the system's default setup:
- For camera support, I needed to install *raspberrypi-firmware-extra* to get start_x.elf installed, after which I enabled it with *start_x=1* in /boot/efi/config.txt
- Added Packman repositories to check fully enabled ffmpeg4.
- Installed *motion* package, nicely the latest 4.4 was available.
- Configured motion to be near the configuration of my Raspbian installation.
- Played around and probably installed various other packages...
- Updated the 200 packages were available.
- Noticed *raspberrypi-firmware-config* really owned the config.txt, and the include extraconfig.txt was there for a reason... then found out [bsc#1192047](https://bugzilla.opensuse.org/show_bug.cgi?id=1192047) and sighed a bit.

![RPi - it boots!](rpi2.jpg)

Some notable differences I noted when switching from Raspbian / Raspberry Pi OS to openSUSE Leap 15.4:

- Huge amount of variety in pre-configured [images](http://download.opensuse.org/distribution/leap/15.4/appliances/)! Enlightement 20, JeOS, minimal X11 (IceWM), GNOME, XFCE, ... everything! I chose the minimal X11 as the starting point, since I did not want GUI other than enabling network.
- Booting looks very familiar to openSUSE user, including the boot menu
- No raspi-config or such is included, configuration is done more on the lower level. OTOH I'm used to it by now.
- No MMAL, but V4L2 compatibility works fine
- Other "standard" raspberry tools are missing and uninstallable from default repositories as well, like vcgencmd.
- *motion* was fresh, but however not compiled with MMAL support. Using /dev/video0 instead.
- *ffmpeg* is recent enough, but compiled without --enable-omx and --enable-omx-rpi

![RPi - good to use with a keyboard/mouse combo](rpi3.jpg)

All in all the first impressions is that openSUSE seems to be well suited for PC like desktop or server use cases in its default configurations. Using more Raspberry Pi specific functionalities may be a bit cumbersome if used to certain tools being available immediately, or the software being compiled with Raspberry Pi specific options. OTOH Raspberry Pi is a lot about hacking so they would not be blockers as such.

I initially had weird network hickups with the wifi, not sure why, so I resorted to wired connectivity for some time. Later on I switched back and wifi seemed fine, possibly thanks to the firmware updates that were installed.

![RPi - infrared camera module works as well :)](rpi4.jpg)

All in all, it requires some hacking but it does work! openSUSE Leap 15.4 as it stands _does_ offer the combination of (so far) stable non-libcamera camera usage (via v4l2 only though), and recent enough ffmpeg. However, the ffmpeg is not compiled with the h264_omx codec possibility for the mkv container which has been my wish. Therefore, it seems that whether using openSUSE or Raspbian, some compiling from sources would be needed to reach the optimal setup.

So far, I've studied most of the topics I wanted to study during the Hackweek. However, long term system stability evaluation requires this to be a longer term experiment, so I will leave the system running for now and monitor it.


---
title: Running Cockpit inside ALP
subtitle: 
date: 2022-12-10T14:00:00+02:00
tags: ["alp", "opensuse", "cockpit"]
type: post
---

ALP - [The Adaptable Linux Platform](https://documentation.suse.com/alp/all/single-html/alp/index.html) – is a new operating system from SUSE to run containerized and virtualized workloads. It is in early prototype phase, but the development is done completely openly so it's easy to jump in to try it.

For this trying out, I used the latest encrypted build – as of the writing, 22.1 – from [ALP images](https://download.opensuse.org/repositories/SUSE:/ALP/images/). I imported it in virt-manager as a Generic Linux 2022 image, using UEFI instead of BIOS, added a TPM device (which I'm interested in otherwise) and referring to an Ignition JSON file in the XML config in virt-manager.

The Ignition part is pretty much fully thanks to Paolo Stivanin who studied the secrets of it before me. But here it goes - and this is required for password login in Cockpit to work in addition to SSH key based login to the VM from host - first, create config.ign file:

```json
{
  "ignition": { "version": "3.3.0" },
  "passwd": {
    "users": [
      {
        "name": "root",
        "passwordHash": "YOURHASH",
        "sshAuthorizedKeys": [
          "ssh-... YOURKEY"
        ]
      }
    ]
  },
  "systemd": {
    "units": [{
      "name": "sshd.service",
      "enabled": true
    }]
  },
  "storage": {
    "files": [
      {
        "overwrite": true,
        "path": "/etc/ssh/sshd_config.d/20-enable-passwords.conf",
        "contents": {
          "source": "data:,PasswordAuthentication%20yes%0APermitRootLogin%20yes%0A"
        },
        "mode": 420
      }
    ]
  }
}
```

...where password SHA512 hash can be obtained using `openssl passwd -6` and the ssh key is your public ssh key.

That file is put to eg /tmp and referred in the virt-manager's XML like follows:

```bash
  <sysinfo type="fwcfg">
    <entry name="opt/com.coreos/config" file="/tmp/config.ign"/>
  </sysinfo>
```

Now we can boot up the VM and ssh in - or you could log in directly too but it's easier to copy-paste commands when using ssh.

Inside the VM, we can follow the ALP documentation to install and start Cockpit:

```bash
podman container runlabel install registry.opensuse.org/suse/alp/workloads/tumbleweed_containerfiles/suse/alp/workloads/cockpit-ws:latest
podman container runlabel --name cockpit-ws run registry.opensuse.org/suse/alp/workloads/tumbleweed_containerfiles/suse/alp/workloads/cockpit-ws:latest
systemctl enable --now cockpit.service
```

Check your host's IP address with `ip -a`, and open IP:9090 in your host's browser:

![Cockpit login screen](cockpit-login.png)

Login with root / your password and you shall get the front page:

![Cockpit front page](cockpit-frontpage.png)

...and many other pages where you can manage your ALP deployment via browser:

![Cockpit podman page](cockpit-subpage.png)

All in all, ALP is in early phases but I'm really happy there's up-to-date documentation provided and people can start experimenting it whenever they want. The images from the linked directory should be fairly good, and [test automation](https://openqa.opensuse.org/group_overview/100) with openQA has been started upon as well.

You can try out the [other](https://documentation.suse.com/alp/all/single-html/alp/index.html#reference-available-alp-workloads) example workloads that are available just as well.

---
title: SUSE at Mindtrek 2024
subtitle: Conference in Tampere, Finland
date: 2024-10-09T12:50:00+03:00
tags: ["suse", "mindtrek", "mindtrek24", "#mindtrek24", "open source", "public sector", "business", "conference"]
type: post
---

[SUSE](https://www.suse.com/) was a gold sponsor at [Mindtrek 2024](https://www.mindtrek.org/mindtrek-conference-2024/), a conference with a long, almost 30 years history starting as a ”multimedia competition”, always with academic conference held alongside it, and more recently held by [COSS – the Finnish Centre for Open Systems and Solutions](https://coss.fi/en/). This year Mindtrek was having a very open source focused program with two tracks, ”The Future of Open Source Business” and ”Enhancing Public Service with Open Source”.

There was a good attendance of 150+ people, a busy feeling and a lot of good talks with participants and fellow sponsors from [DigiFinland](https://digifinland.fi/en/), [Tampere University](https://www.tuni.fi/en), [Seravo](https://seravo.com/en/), [Druid](https://druid.fi/en/), [HH Partners](https://www.hhpartners.fi/en), [doubleOpen()](https://www.doubleopen.org/), [Metatavu](https://metatavu.fi/en/), [Opinsys](https://opinsys.com/contact/), [City of Tampere](https://www.tampere.fi/en), [Haltu](https://www.haltu.com/) and [itewiki](https://www.itewiki.fi/).

The conference started with welcome words and presenting the recipient of the 2024 Open World Hero award, which this year went to [eVaka project](https://github.com/espoon-voltti/evaka) – ERP for early childhood education – which is a great success story on open source, developing based on immediate needs and starting with a minimum viable product, collaboration between big cities on common goals, and now further adoption by smaller municipilaties, which driven by open source companies which can deploy and maintain them. They also later had a talk about how they achieved their goals and continue forward, on the principles of good software development practices, early user feedback and a lot of other signs of a good project. The recipients were on the behalf of cities of Espoo and Tampere.

![eVaka project members receiving Open World Hero 2024 award](openworldhero.jpg)

The keynote speaker before the tracks was [Sachiko Muto](https://en.wikipedia.org/wiki/Sachiko_Muto) from OpenForum Europe & RISE withe her presentation reflecting on how open source has both risen to the top and also still has quite some ways to go in terms of procurement, awareness, skills and so on. The presentation is quite hard to summarize but was well thought out.

![Sachiko Muto on building European digital sovereignty](sachiko1.jpg)
![Sachiko Muto on the role of open source](sachiko2.jpg)

Then we had SUSE's Emiel Brok giving an energized presentation about the ”perfect storm” coming and how SUSE products can help with for example requirements of [NIS-2](https://www.nis-2-directive.com/) and had a bunch of good talks. It was nicely put together, entertaining, and positioned SUSE in a very positive light in what it has to offer.

We also had a SUSE booth which was busy througout the day, with people asking about SUSE, and distributing a lot of chameleons to happy receivers.

![Emiel Brok from SUSE](suse1.jpg)
![Emiel Brok from SUSE, photo 2](suse2.jpg)
![SUSE booth](suse3.jpg)

Throughout the rest of the day I was at the booth, and also followed at least the eVaka project presentation and another interesting presentation on NIS2 and Cyber Resilience Act from Martin von Willebrand. There was also a small evening event to continue open source discussions in a more relaxed environment.
